import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Button, Platform, Text,
  SafeAreaView, 
  Image,
  StatusBar,
  Pressable,
  Alert,
} from 'react-native';
import { Html5QrcodeScanType, Html5QrcodeScanner } from 'html5-qrcode';
import { Camera } from 'expo-camera';
import { Header } from 'react-native/Libraries/NewAppScreen';
import { Footer } from './src/components/basics/footer';
import { styles } from './src/screens/selfqr-scanner/SelfqrScannerStyles';
import { NavigationProp, useRoute } from "@react-navigation/native";
import { HiraginoKakuText } from './src/components/StyledText';

const App = () => {
  const [decodedResults, setDecodedResults] = useState<string[]>([]);
  const [isCameraPaused, setIsCameraPaused] = useState(false);
  let qrCodeScanner: Html5QrcodeScanner | undefined;

  useEffect(() => {
    if (Platform.OS === 'web') {
      const config = {
        fps: 10,
        qrbox: { width: 500, height: 500 },
        rememberLastUsedCamera: true,
        supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_CAMERA],
      };

      qrCodeScanner = new Html5QrcodeScanner(
        "reader",
        config,
        false
      );
      qrCodeScanner.render(qrCodeSuccessCallback, qrCodeErrorCallback);
    }
  }, []);

  const qrCodeSuccessCallback = (decodedText: string) => {
    console.log("Decoded text:", decodedText);
    setDecodedResults((prev) => [...prev, decodedText]);
    setIsCameraPaused(true);
  };

  const qrCodeErrorCallback = (errorMessage: string) => {
    console.error("QR code scan error:", errorMessage);
    // Handle QR code scan error
  };

  const resumeCamera = () => {
    setIsCameraPaused(false);
    if (Platform.OS === 'web' && qrCodeScanner) {
      qrCodeScanner.clear();
      qrCodeScanner.render(qrCodeSuccessCallback, qrCodeErrorCallback);
    }
  };

  return (
    <View style={styles.container}>
      {!isCameraPaused ? (
        Platform.OS === 'ios' ? (
          <ExpoCameraComponent onBarCodeScanned={qrCodeSuccessCallback} />
        ) : (
          <View id="reader" style={{ width: "100%", height: "100%" }} />
        )
      ) : (
        <View>
          <Button title="Resume Camera" onPress={resumeCamera} />
        </View>
      )}
    </View>
  );
};

interface ExpoCameraProps {
  onBarCodeScanned: (data: string) => void;
}

const ExpoCameraComponent: React.FC<ExpoCameraProps> = ({ onBarCodeScanned }) => {
  const [hasPermission, setHasPermission] = useState<boolean | null>(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
  }, []);

  if (hasPermission === null) {
    return <View />;
  }
  if (!hasPermission) {
    return <Text>No access to camera</Text>;
  }

  const handleSelectReceptionMethod = () => {
    // navigation.navigate("SelectReceptionMethod", {
    //   userId: "mec",
    // });
  };

  const handleBarcodeScanned = ({ data }: { data: string }) => {
    if (!scanned) {
      setScanned(true);
      Alert.alert(
        "QR Code Scanned",
        `Bar code with data ${data} has been scanned!`,
        [
          {
            text: "OK",
            onPress: () => {
              setScanned(false);
            },
          },
        ],
        { cancelable: false }
      );
    }
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="自己QRをかざしてください"
        buttonName="受付をやめる"
        onPress={handleSelectReceptionMethod}
      />
      <View style={styles.container}>
        <View style={styles.leftSide}>
          {/* <Image
            source={require("./assets/images/qrScanner.png")}
            style={styles.image}
          /> */}
        </View>
        <View style={styles.rightSide}>
          <View style={styles.camera}></View>
          {hasPermission === true && (
            <Camera
              style={styles.camera}
              type={"back" as any}
              onBarCodeScanned={handleBarcodeScanned}
            />
          )}
          <View style={[styles.corner, styles.topLeftCorner]} />
          <View style={[styles.corner, styles.topRightCorner]} />
          <View style={[styles.corner, styles.bottomLeftCorner]} />
          <View style={[styles.corner, styles.bottomRightCorner]} />
          <View style={styles.overlay}>
            <View style={styles.buttonContainer}>
              <Pressable
                style={styles.receptionByOne}
              >
                <HiraginoKakuText normal>1人で受付</HiraginoKakuText>
              </Pressable>
              <Pressable
                style={styles.receptionByFamily}
              >
                <HiraginoKakuText normal>家族で受付</HiraginoKakuText>
              </Pressable>
            </View>
          </View>
        </View>
      </View>
      <Footer
        hasNextButton={false}
      ></Footer>
    </SafeAreaView>
  );
};

export default App;
