import React from "react";
import Dialog from "../../components/basics/Dialog";
import { View } from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import styles from "./PreReceptionVerificationStyles";

type PreVerificationProps = {
  eventName: string;
  eventPeriod: string;
  venue: string,
  eventDetailId: number;
  venueId: number;
  onFirstButtonPress?: () => void;
  onSecondButtonPress?: () => void;
};

export const PreReceptionVerification = (props: PreVerificationProps) => {

  // console.log("EventId: ", props.eventDetailId, "VenueId: ", props.venueId);

  return (
    <Dialog
      dialogTitle="受付を開始しますか？"
      text="ログアウトしますか？"
      firstButtonText="受付開始"
      iconVisible={false}
      secondButtonVisible={true}
      secondButtonText="キャンセル"
      containerHeight={493}
      containerGap={32}
      dialogBodyGap={40}
      btnContainerHeight={120}
      onFirstButtonPress={props.onFirstButtonPress}
      onSecondButtonPress={props.onSecondButtonPress}
    >
      <View style={styles.ListContainter}>
        <HiraginoKakuText style={styles.subTitleText}>
          {props.eventName}
        </HiraginoKakuText>
        <View style={styles.upperContainer}>
          <HiraginoKakuText style={styles.innerLabel}>会場</HiraginoKakuText>
          <HiraginoKakuText style={styles.innerText} normal>
            {props.venue}
          </HiraginoKakuText>
        </View>
        <View style={styles.LowerContainer}>
          <HiraginoKakuText style={styles.innerLabel}>
            イベント期間
          </HiraginoKakuText>
          <HiraginoKakuText style={styles.innerText} normal>
            {props.eventPeriod}
          </HiraginoKakuText>
        </View>
      </View>
    </Dialog>
  );
};
