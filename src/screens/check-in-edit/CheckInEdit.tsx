import React, { useState, useRef, useEffect } from "react";
import { StatusBar, SafeAreaView, Pressable, TextInput, Keyboard, TouchableWithoutFeedback } from "react-native";
import styles from "./CheckInEditStyle";
import { Header } from "../../components/basics/header";
import { View } from "../../components/Themed";
import { Footer } from "../../components/basics/footer";
import { HiraginoKakuText } from "../../components/StyledText";
import { MaterialIcons } from "@expo/vector-icons";
import { colors } from "../../styles/color";
import { Button } from "../../components/basics/Button";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { CustomCalendar } from "../../components/basics/Calendar";
import { format, parse } from "date-fns";

type Props = {
  navigation: NavigationProp<any, any>;
};

type Params = {
  eventId: number,
  venueId: number,
  receptMethodCode: number,
  surname: string,
  givenName: string,
  kanaSurname: string,
  kanaGivenName: string,
  dateOfBirth: string,
  genderCode: number,
  postCode: string,
  juuSho: string,
}

export const CheckInEdit = ({ navigation }: Props) => {
  const route = useRoute();
  const { eventId, venueId, receptMethodCode } = route.params as Params;
  const { surname, givenName, kanaSurname, kanaGivenName, dateOfBirth, genderCode, postCode, juuSho } = route.params as Params;
  // FORMAT birthDate
  const formattedDate = format(parse(dateOfBirth, "yyyy年MM月dd日", new Date()), "yyyy/MM/dd");
  const [selectedOption, setSelectedOption] = useState('');
  const [selectedOptionCode, setSelectedOptionCode] = useState(genderCode);
  const [isBirthDateCalendarVisible, setBirthDateCalendarVisible] = useState(false);

  const [firstName, setFirstName] = useState(givenName);
  const [lastName, setLastName] = useState(surname);
  const [kanaFirstName, setkanaFirstName] = useState(kanaGivenName);
  const [kanaLastName, setKanaLastName] = useState(kanaSurname);
  const [birthDate, setBirthDate] = useState(formattedDate);
  const [zipCode, setZipCode] = useState(postCode);
  const [address, setAddress] = useState(juuSho);
  const [postCodeAddress, setPostCodeAddress] = useState('');

  // ERR Msg
  const [firstNameErrMsg, setFirstNameErrMsg] = useState('');
  const [lastNameErrMsg, setLastNameErrMsg] = useState('');
  const [kanaFirstNameErrMsg, setkanaFirstNameErrMsg] = useState('');
  const [kanaLastNameErrMsg, setKanaLastNameErrMsg] = useState('');
  const [zipCodeErrMsg, setZipCodeErrMsg] = useState("");
  const [addressErrMsg, setAddressErrMsg] = useState("");

  const birthDateInputRef = useRef(null);
  const birthDateRef = useRef(null);

  useEffect(() => {
    if (postCodeAddress) {
      setAddress(postCodeAddress);
    }

    if (!genderCode && !selectedOption) {
      setSelectedOptionCode(0);
      setSelectedOption("回答しない");
    }
  }, [postCodeAddress, genderCode]);

  // SEARCH ZipCode Address
  const searchAddressWithPostalCode = async () => {
    try {
      const response = await fetch(`https://zipcloud.ibsnet.co.jp/api/search?zipcode=${zipCode}`);
      const data = await response.json();
      if (data.results && data.results.length > 0) {
        setPostCodeAddress(data.results[0].address1 + data.results[0].address2 + data.results[0].address3);
        setAddress(postCodeAddress);
      } else {
        setAddressErrMsg('この郵便番号は使用できません');
      }
    } catch (error) {
      console.error('Error fetching address:', error);
      setAddressErrMsg('現在、郵便番号による住所検索はご利用いただけません');
    }
  };

  const handleGenderOption = (code: number, value: string) => {
    setSelectedOptionCode(code);
    setSelectedOption(value);
  };

  const handleReturnCheckInConfirmation = () => {
    if (firstName && lastName &&
      isKatakana(kanaFirstName) && isKatakana(kanaLastName) &&
      zipCode && isValidZipCode() &&
      address
    ) {
      navigation.navigate("CheckInConfirmation", {
        eventId: eventId,
        venueId: venueId,
        receptMethodCode: receptMethodCode,
        surname: lastName.trim(),
        givenName: firstName.trim(),
        kanaSurname: kanaLastName.trim(),
        kanaGivenName: kanaFirstName.trim(),
        birthDate: format(parse(birthDate.trim(), "yyyy/MM/dd", new Date()), "yyyy年MM月dd日"),
        genderCode: selectedOptionCode,
        postCode: zipCode.trim(),
        address: address.trim(),
        isNameUpdated: isNameUpdated(),
        isKanaNameUpdated: isKanaNameUpdated(),
        isBirthDateUpdated: isBirthDateUpdated(),
        isGenderUpdated: isGenderUpdated(),
        isZipcodeUpdated: isZipcodeUpdated(),
        isAddressUpdated: isAddressUpdated(),
      });
    } else {
      handleFirstNameBlur();
      handleLastNameBlur();
      handleKanaFirstName();
      handleKanaLastName();
      isValidZipCode();
      handleAddressBlur();
    }
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
      eventId: eventId,
      venueId: venueId,
    });
  };

  const handleBirthDateCalendarPress = (event: any) => {
    (birthDateInputRef.current as any).focus();
    setBirthDateCalendarVisible(!isBirthDateCalendarVisible);
  };

  const handleBirthDateSelect = (date: any) => {
    setBirthDate(format(parse(date, "yyyy-MM-dd", new Date()), "yyyy/MM/dd"));
    setBirthDateCalendarVisible(false);
  };

  const closeCalendar = (event: any) => {
    if (event.nativeEvent.target != birthDateInputRef.current && event.nativeEvent.target != birthDateRef) {
      if (isBirthDateCalendarVisible) {
        setBirthDateCalendarVisible(false);
      }
    }
  };

  // HANDLE Err Msg
  const isKatakana = (text: string) => {
    const katakanaRegex = /^[\u30A0-\u30FF]+$/;
    return katakanaRegex.test(text);
  };

  const handleFirstNameBlur = () => {
    if (!firstName) {
      setFirstNameErrMsg("お名前を入力してください");
    } else {
      setFirstNameErrMsg("");
    }
  }

  const handleLastNameBlur = () => {
    if (!lastName) {
      setLastNameErrMsg("お名前を入力してください");
    } else {
      setLastNameErrMsg("");
    }
  }

  const handleKanaFirstName = () => {
    if (!kanaFirstName) {
      setkanaFirstNameErrMsg("お名前（カナ）を入力してください");
    } else if (!isKatakana(kanaFirstName)) {
      setkanaFirstNameErrMsg("カタカナで入力してください");
    } else {
      setkanaFirstNameErrMsg("");
    }
  }

  const handleKanaLastName = () => {
    if (!kanaLastName) {
      setKanaLastNameErrMsg("お名前（カナ）を入力してください");
    } else if (!isKatakana(kanaLastName)) {
      setKanaLastNameErrMsg("カタカナで入力してください");
    } else {
      setKanaLastNameErrMsg("");
    }
  }

  const isValidZipCode = () => {
    if (!zipCode) {
      setZipCodeErrMsg("郵便番号を入力してください");
    } else if (!/^\d{3}-\d{4}$/.test(zipCode)) {
      setZipCodeErrMsg("郵便番号は数字7桁で入力してください");
    } else if (/^(.)\1{2}-\1{4}$/.test(zipCode)) {
      setZipCodeErrMsg("この郵便番号は使用できません");
    } else {
      setZipCodeErrMsg("");
    }
    return !zipCodeErrMsg;
  }

  const handleAddressBlur = () => {
    if (!address) {
      setAddressErrMsg("住所を入力してください");
    } else {
      setAddressErrMsg("");
    }
  }

  // HANDLE Update Flag
  const isNameUpdated = () => {
    const isChanged = surname.trim() !== lastName.trim() || givenName.trim() !== firstName.trim();
    return isChanged;
  };

  const isKanaNameUpdated = () => {
    const isChanged = kanaSurname.trim() !== kanaLastName.trim() || kanaGivenName.trim() !== kanaFirstName.trim();
    return isChanged;
  };

  const isBirthDateUpdated = () => {
    const isChanged = formattedDate !== birthDate;
    return isChanged;
  }

  const isGenderUpdated = () => {
    const isChanged = genderCode !== selectedOptionCode;
    return isChanged;
  }

  const isZipcodeUpdated = () => {
    const isChanged = postCode !== zipCode;
    return isChanged;
  }

  const isAddressUpdated = () => {
    const isChanged = juuSho.trim() !== address.trim();
    return isChanged;
  }

  return (
    <KeyboardAwareScrollView
      style={{ flex: 1, width: "100%" }}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={styles.mainContainer}
      scrollEnabled={false}
    >
      <TouchableWithoutFeedback onPress={closeCalendar}>
        <SafeAreaView style={styles.mainContainer}>
          <StatusBar barStyle="dark-content" />
          <Header
            titleName="受付内容修正"
            buttonName="受付をやめる"
            onPress={handleSelectReceptionMethod}
          />
          <View style={styles.bodyContainer}>
            <HiraginoKakuText style={styles.titleText}>
              受付内容を修正してください
            </HiraginoKakuText>
            <View style={styles.container}>
              <View style={styles.nameLabelContainer}>
                <HiraginoKakuText style={styles.labelNameText}>
                  お名前
                </HiraginoKakuText>
                <View>
                  <View style={styles.nameInputsContainer}>
                    <TextInput
                      style={[styles.FirstNameInput, styles.FirstNameInputText]}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                      value={firstName}
                      onChangeText={text => setFirstName(text)}
                      onBlur={isNameUpdated}
                    />
                    <TextInput
                      style={[styles.LastNameInput, styles.LastNameInputText]}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                      value={lastName}
                      onChangeText={text => setLastName(text)}
                      onBlur={isNameUpdated}
                    />
                  </View>
                  <View style={styles.nameInputsContainer}>
                    <HiraginoKakuText style={styles.errorText} normal>
                      {firstNameErrMsg}
                    </HiraginoKakuText>
                    <HiraginoKakuText style={styles.errorText} normal>
                      {lastNameErrMsg}
                    </HiraginoKakuText>
                  </View>
                </View>
              </View>
              <View style={styles.nameLabelContainer}>
                <HiraginoKakuText style={styles.labelNameText}>
                  お名前（カナ）
                </HiraginoKakuText>
                <View>
                  <View style={styles.nameInputsContainer}>
                    <TextInput
                      style={[styles.FirstNameInput, styles.FirstNameInputText]}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                      value={kanaFirstName}
                      onChangeText={text => setkanaFirstName(text)}
                      onBlur={isKanaNameUpdated}
                    />
                    <TextInput
                      style={[styles.LastNameInput, styles.LastNameInputText]}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                      value={kanaLastName}
                      onChangeText={text => setKanaLastName(text)}
                      onBlur={isKanaNameUpdated}
                    />
                  </View>
                  <View style={styles.nameInputsContainer}>
                    <HiraginoKakuText style={styles.errorText} normal>
                      {kanaFirstNameErrMsg}
                    </HiraginoKakuText>
                    <HiraginoKakuText style={styles.errorText} normal>
                      {kanaLastNameErrMsg}
                    </HiraginoKakuText>
                  </View>
                </View>
              </View>
              <View style={styles.birthDateContainer}>
                <HiraginoKakuText style={styles.labelBirthDateText}>
                  生年月日
                </HiraginoKakuText>
                <View style={styles.birthDateInputsContainer}>
                  <View style={styles.birthDateInput}>
                    <TextInput
                      ref={birthDateInputRef}
                      style={styles.birthDateInputText}
                      value={birthDate != "" ? format(new Date(birthDate), "yyyy/MM/dd") : birthDate}
                      onPressIn={handleBirthDateCalendarPress}
                      onPointerDown={handleBirthDateCalendarPress}
                      showSoftInputOnFocus={false}
                      onTouchStart={() => Keyboard.dismiss()}
                      editable={false}
                      onBlur={isBirthDateUpdated}
                    />
                    <Pressable style={styles.calendarIconContainer}
                      ref={birthDateRef}
                      onPress={handleBirthDateCalendarPress}>
                      <MaterialIcons
                        name="calendar-today"
                        size={22}
                        color={colors.activeCarouselColor}
                        style={styles.calendarIcon}
                      />
                    </Pressable>
                    {isBirthDateCalendarVisible && (
                      <CustomCalendar selectedDate={birthDate} onDateSelect={handleBirthDateSelect} />
                    )}
                  </View>
                </View>
              </View>
              <View style={styles.genderContainer}>
                <HiraginoKakuText style={styles.labelGenderText}>
                  性別
                </HiraginoKakuText>
                <View style={styles.genderRadioContainer}>
                  <View style={styles.radioContainer}>
                    <RadioPanel
                      selected={selectedOptionCode === 1}
                      onPress={() => handleGenderOption(1, "男性")}
                      radioBtnText="男性"
                    />
                  </View>
                  <View style={styles.radioContainer}>
                    <RadioPanel
                      selected={selectedOptionCode === 2}
                      onPress={() => handleGenderOption(2, "女性")}
                      radioBtnText="女性"
                    />
                  </View>
                  <View
                    style={[styles.radioContainer, styles.radioKaitouContainer]}
                  >
                    <RadioPanel
                      selected={selectedOptionCode === 0}
                      onPress={() => handleGenderOption(0, "回答しない")}
                      radioBtnText="回答しない"
                    />
                  </View>
                </View>
              </View>
              <View style={styles.postCodeContainer}>
                <HiraginoKakuText style={styles.labelPostCodeText}>
                  郵便番号
                </HiraginoKakuText>
                <View style={{ backgroundColor: "rgb(255, 255, 255)" }}>
                  <View style={styles.postCodeInputsContainer}>
                    <TextInput
                      style={[styles.postCodeInput, styles.postCodeInputText]}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                      value={zipCode}
                      onChangeText={text => {
                        const regex = /^[0-9-]*$/;
                        if (text.length <= 8 && regex.test(text)) {
                          if (text.length === 4 && text.charAt(3) !== '-') {
                            setZipCode(text.slice(0, 3) + "-" + text.slice(3));
                          } else {
                            setZipCode(text);
                          }
                        }
                      }}
                      keyboardType="numeric"
                      maxLength={8}
                      onBlur={() => {
                        isValidZipCode();
                        isZipcodeUpdated();
                      }}
                    />
                    <Button
                      text="住所検索"
                      onPress={searchAddressWithPostalCode}
                      style={styles.searchButton}
                      type="ButtonMSecondary"
                    />
                  </View>
                  <HiraginoKakuText style={styles.errorText} normal>
                    {zipCodeErrMsg}
                  </HiraginoKakuText>
                </View>
              </View>
              <View style={styles.addressContainer}>
                <HiraginoKakuText style={styles.labelAddressText}>
                  住所
                </HiraginoKakuText>
                <View style={{ backgroundColor: "rgb(255, 255, 255)" }}>
                  <TextInput
                    style={[styles.addressInput, styles.addressInputText]}
                    onFocus={() => setBirthDateCalendarVisible(false)}
                    value={address}
                    onChangeText={text => setAddress(text)}
                    onBlur={isAddressUpdated}
                  />
                  <HiraginoKakuText style={styles.errorText} normal>
                    {addressErrMsg}
                  </HiraginoKakuText>
                </View>
              </View>
            </View>
          </View>
          <Footer
            rightButtonText="確認する"
            hasPreviousButton={false}
            onPressNext={handleReturnCheckInConfirmation}
            style={{ zIndex: -1 }}
          ></Footer>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    </KeyboardAwareScrollView>
  );
};

const RadioButton = (props: any) => {
  return (
    <View
      style={[
        {
          height: 24,
          width: 24,
          borderRadius: 12,
          borderWidth: 2,
          borderColor: colors.fullyTransparentBlack,
          alignItems: "center",
          justifyContent: "center",
        },
        props.style,
      ]}
    >
      {props.selected ? (
        <View
          style={{
            height: 10,
            width: 10,
            borderRadius: 6,
            backgroundColor: colors.primary,
          }}
        />
      ) : null}
    </View>
  );
};

interface RadioPanelProps {
  selected: boolean;
  onPress: () => void;
  radioBtnText: string;
}

const RadioPanel = ({ selected, onPress, radioBtnText }: RadioPanelProps) => {
  return (
    <Pressable onPress={onPress} style={styles.radioPressable}>
      <View style={styles.radioButtonIcon}>
        <RadioButton
          selected={selected}
          style={[styles.radioButton, selected && styles.selectedRadioButton]}
        />
      </View>
      <View style={styles.radioTextContainer}>
        <HiraginoKakuText style={styles.radioText} normal>
          {radioBtnText}
        </HiraginoKakuText>
      </View>
    </Pressable>
  );
};
