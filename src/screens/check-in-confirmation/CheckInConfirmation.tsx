import React, { useEffect, useState } from "react";
import { SafeAreaView, View, Dimensions, ScrollView } from "react-native";
import styles from "./CheckInConfirmationStyles";
import { StatusBar } from "react-native";
import { Header } from "../../components/basics/header";
import { HiraginoKakuText } from "../../components/StyledText";
import { Footer } from "../../components/basics/footer";
import { Button } from "../../components/basics/Button";
import { MaterialIcons } from "@expo/vector-icons";
import { colors } from "../../styles/color";
import Completion from "../completion/Completion";
import { NavigationProp, useRoute } from "@react-navigation/native";

type Props = {
  navigation: NavigationProp<any, any>;
};

type Params = {
  eventId: number,
  venueId: number,
  receptMethodCode: number,
  surname: string,
  givenName: string,
  kanaSurname: string,
  kanaGivenName: string,
  birthDate: string,
  genderCode: number,
  postCode: string,
  address: string,
  isNameUpdated: boolean,
  isKanaNameUpdated: boolean,
  isBirthDateUpdated: boolean,
  isGenderUpdated: boolean,
  isZipcodeUpdated: boolean,
  isAddressUpdated: boolean,
}

export const CheckInConfirmation = ({ navigation }: Props) => {
  const route = useRoute();
  const { eventId, venueId, receptMethodCode } = route.params as Params;
  const { surname, givenName, kanaSurname, kanaGivenName, birthDate, genderCode, postCode, address } = route.params as Params;
  const { isNameUpdated, isKanaNameUpdated, isBirthDateUpdated, isGenderUpdated, isZipcodeUpdated, isAddressUpdated } = route.params as Params;
  const [isModalVisible, setModalVisible] = useState(false);
  const [scrollEnabled, setScrollEnabled] = useState(false);
  const [gender, setGender] = useState('');

  const handleCompletion = () => {
    openCompletionModal();
  };

  useEffect(() => {
    if (isModalVisible === true) {
      let timeOut = setTimeout(() => {
        closeModal();
        navigation.navigate("SelectReceptionMethod", {
          userId: "mec",
        });
      }, 10000);
      return () => clearTimeout(timeOut);
    }

    // SET Gender
    switch (genderCode) {
      case 1:
        setGender("男性");
        break;
      case 2:
        setGender("女性");
        break;
      default:
        setGender("回答しない");
    }
  }, [genderCode]);

  const openCompletionModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  const handleEdit = () => {
    navigation.navigate("CheckInEdit", {
      eventId: eventId,
      venueId: venueId,
      receptMethodCode: receptMethodCode,
      surname: surname,
      givenName: givenName,
      kanaSurname: kanaSurname,
      kanaGivenName: kanaGivenName,
      dateOfBirth: birthDate,
      genderCode: genderCode,
      postCode: postCode,
      juuSho: address,
    });
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  const onLayoutHandler = (e: any) => {
    var { height } = e.nativeEvent.layout;

    if (height > 400) {
      setScrollEnabled(true);
    } else {
      setScrollEnabled(false);
    }
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content"></StatusBar>
      <Header
        titleName="受付内容確認"
        buttonName="受付をやめる"
        onPress={handleSelectReceptionMethod}
      ></Header>
      <ScrollView scrollEnabled={scrollEnabled}>
        <View style={styles.bodyContainer}>
          <View style={styles.innerMainTitle}>
            <HiraginoKakuText style={styles.innerMainTitleText}>
              この内容で受付しますか？
            </HiraginoKakuText>
          </View>

          <View style={styles.innerBodyContainer} onLayout={onLayoutHandler}>
            <View style={styles.bodyTitle}>
              <HiraginoKakuText style={styles.bodyTitleText}>
                受付内容
              </HiraginoKakuText>
              <View style={styles.buttonContainer}>
                <Button
                  text="内容を修正する"
                  type="ButtonMSecondary"
                  style={styles.btnModify}
                  icon={
                    <MaterialIcons
                      name="mode-edit"
                      size={24}
                      color={colors.primary}
                      style={styles.iconStyle}
                    />
                  }
                  iconPosition="behind"
                  onPress={handleEdit}
                ></Button>
              </View>
            </View>
            <View style={styles.rowGroup}>
              <View style={styles.row}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      お名前
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      !isNameUpdated
                        ? styles.secondContent
                        : styles.secondContentCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {givenName} {surname} 
                    </HiraginoKakuText>
                  </View>
                </View>
                {isNameUpdated && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>

              <View style={styles.row}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      お名前（カナ）
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      !isKanaNameUpdated
                        ? styles.secondContent
                        : styles.secondContentCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {kanaGivenName} {kanaSurname} 
                    </HiraginoKakuText>
                  </View>
                </View>
                {isKanaNameUpdated && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>

              <View style={styles.row}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      生年月日
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      !isBirthDateUpdated
                        ? styles.secondContent
                        : styles.secondContentCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {birthDate}
                    </HiraginoKakuText>
                  </View>
                </View>
                {isBirthDateUpdated && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>
              {/* // 性別(Female/male/other) is optional. */}
              <View style={styles.row}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      性別
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      !isGenderUpdated
                        ? styles.secondContent
                        : styles.secondContentCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {gender}
                    </HiraginoKakuText>
                  </View>
                </View>
                {isGenderUpdated && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>

              <View style={styles.row}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      郵便番号
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      !isZipcodeUpdated
                        ? styles.secondContent
                        : styles.secondContentCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {postCode}
                    </HiraginoKakuText>
                  </View>
                </View>
                {isZipcodeUpdated && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>

              <View style={styles.rowAddress}>
                <View style={styles.rowContent}>
                  <View style={styles.firstContent}>
                    <HiraginoKakuText style={styles.innerBodyBoldText}>
                      住所
                    </HiraginoKakuText>
                  </View>
                  <View
                    style={
                      !isAddressUpdated
                        ? styles.secondContentAddress
                        : styles.secondContentAddressCorrected
                    }
                  >
                    <HiraginoKakuText style={styles.innerBodyText} normal>
                      {address}
                    </HiraginoKakuText>
                  </View>
                </View>
                {isAddressUpdated && (
                  <View style={styles.correctedBadge}>
                    <HiraginoKakuText style={styles.correctedText}>
                      修正済
                    </HiraginoKakuText>
                  </View>
                )}
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <Footer
        rightButtonText="受付する"
        hasPreviousButton={false}
        showNextIcon={false}
        onPressNext={handleCompletion}
      ></Footer>
      {isModalVisible && <Completion closeModal={closeModal} />}
    </SafeAreaView>
  );
};

export default CheckInConfirmation;
