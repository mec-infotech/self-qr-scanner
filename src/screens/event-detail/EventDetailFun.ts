import { executeQuery } from "../../aws/db/dbOperation";

// GET VenueName
export const getVenuName = async (eventDetailId: number) => {
    const method = 'POST';
    const queryString = "SELECT name FROM venue where event_id = " + eventDetailId + ";";
    return executeQuery(method, queryString);
}

// GET VenueId
export const getVenueId = async (venueName: string) => {
    const method = 'POST';
    const queryString = "SELECT venue_id FROM venue Where name = '" + venueName + "';";
    return executeQuery(method, queryString);
}