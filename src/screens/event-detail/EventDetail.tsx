import React, { useEffect, useState } from "react";
import { View, Pressable, SafeAreaView, StatusBar } from "react-native";
import { styles } from "./EventDetailStyles";
import { Header } from "../../components/basics/header";
import { Footer } from "../../components/basics/footer";
import { HiraginoKakuText } from "../../components/StyledText";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { PreReceptionVerification } from "../pre-reception-verification/PreReceptionVerification";
import {getVenuName, getVenueId } from "./EventDetailFun";

// JSON
import venueData from './venue.json';

type Props = {
  navigation: NavigationProp<any, any>;
};

type Params = {
  userId: string;
  eventName: string;
  eventPeriod: string;
  eventId: number;
}

export const EventDetail = ({ navigation }: Props) => {
  const route = useRoute();
  const { userId, eventId, eventName, eventPeriod } = route.params as Params;

  const [selectedVenue, setselectedVenue] = useState('');
  const [isPreReceptionModalVisible, setIsPreReceptionModalVisible] = useState(false);
  const [venueNames, setVenueNames] = useState<any[]>([]);
  const [radioErrMsg, setRadioErrMsg] = useState('');
  const [venueId, setVenueId] = useState(0);

  let eventTime = '';
  // CHECK EventTime
  if (!eventPeriod) {
    eventTime = "指定なし";
  } else {
    eventTime = eventPeriod;
  }

  // GET VenueName
  const fetchVenueName = async () => {
    if(eventId){
      const result = await getVenuName(eventId);
      return result.data;
    } else {
      console.log("Error: イベントIDまたは会場IDには値がありません")
    }
  };

  const fetchId = async (venueName: string) => {
    // GET VenueId
    const result2 = await getVenueId(venueName);
    setVenueId(result2.data[0].venue_id);
  };

  const handleSelectedVenue = async (option: string) => {    
    if (option) {
      setselectedVenue(option);
      await fetchId(option);
    }
  };

  const handleBack = () => {
    navigation.navigate("EventList", {
      userId: userId,
    });
  };

  const handleNext = () => {    
    if (selectedVenue) {
      // if (eventId && venueId) {
      //   setIsPreReceptionModalVisible(true);
      // } else {
      //   console.log("Error: イベントIDまたは会場IDには値がありません");
      // }
      
      //Json
      setIsPreReceptionModalVisible(true);
    } else {
      setRadioErrMsg("会場を選択してください");
    }
  };

  const handleAccept = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: userId,
      eventId: eventId,
      venueId: venueId,
    });
    setIsPreReceptionModalVisible(false);
  };

  const handleCancel = () => {
    setIsPreReceptionModalVisible(false);
  };


  useEffect(() => {
    const fetchData = async () => {
      const result = await fetchVenueName();
      setVenueNames(result);
    };

    fetchData();
  }, []);

  return (
    <SafeAreaView style={styles.detailMainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header titleName="詳細" buttonName="" hasButton={false} />
      <View style={styles.detailContainer}>
        <View style={styles.detailBodyContainer}>
          <View style={styles.detailOuterFrame1}>
            <HiraginoKakuText style={styles.subTitle}>
              {eventName}
            </HiraginoKakuText>
            <View style={styles.detailInnerFrame1}>
              <HiraginoKakuText style={styles.eventText} normal>
                イベント期間：
              </HiraginoKakuText>
              <HiraginoKakuText style={styles.eventText} normal>
                {eventTime}
              </HiraginoKakuText>
            </View>
          </View>

          <View style={styles.detailLineBreak}></View>

          <View style={styles.detailOuterFrame2}>
            <View style={styles.detailInnerFrame2}>
              <HiraginoKakuText style={styles.sentakuTitleText}>
                会場
              </HiraginoKakuText>
              <HiraginoKakuText style={styles.sentakuSubTitleText} normal>
                受付する会場を選択してください。
              </HiraginoKakuText>
            </View>

            <HiraginoKakuText style={styles.errorText} normal>
              {radioErrMsg}
            </HiraginoKakuText>

            {/* {venueNames.map((venue, index) => (
              <RadioPanel
                key={index}
                selected={selectedVenue === venue.name}
                onPress={() => handleSelectedVenue(venue.name)}
                radioBtnText={venue.name}
              />
            ))} */}

            {/* Json */}
            {venueData.data.map((venue, index) => (
              <RadioPanel
                key={index}
                selected={selectedVenue === venue}
                onPress={() => handleSelectedVenue(venue)}
                radioBtnText={venue}
              />
            ))}

          </View>
        </View>
      </View>
      <Footer
        rightButtonText="受付開始"
        showNextIcon={false}
        onPressPrevious={handleBack}
        onPressNext={handleNext}
      ></Footer>
      {isPreReceptionModalVisible && (
        <PreReceptionVerification
          eventName={eventName}
          eventPeriod={eventPeriod}
          venue={selectedVenue}
          eventDetailId={eventId}
          venueId={venueId}
          onFirstButtonPress={handleAccept}
          onSecondButtonPress={handleCancel}
        />
      )}
    </SafeAreaView>
  );
};

const RadioButton = (props: any) => {
  return (
    <View
      style={[
        {
          height: 24,
          width: 24,
          borderRadius: 12,
          borderWidth: 2,
          borderColor: "#000",
          alignItems: "center",
          justifyContent: "center",
        },
        props.style,
      ]}
    >
      {props.selected ? (
        <View
          style={{
            height: 10,
            width: 10,
            borderRadius: 6,
            backgroundColor: "#346DF4",
          }}
        />
      ) : null}
    </View>
  );
};

interface RadioPanelProps {
  selected: boolean;
  onPress: () => void;
  radioBtnText: string;
}

const RadioPanel = ({ selected, onPress, radioBtnText }: RadioPanelProps) => {
  return (
    <View style={[styles.radioPanel, selected && styles.selectedRadioPanel]}>
      <Pressable onPress={onPress} style={styles.radioPressable}>
        <View style={styles.radioButtonIcon}>
          <RadioButton
            selected={selected}
            style={[styles.radioButton, selected && styles.selectedRadioButton]}
          />
        </View>
        <View style={styles.radioTextContainer}>
          <HiraginoKakuText style={styles.radioText}>
            {radioBtnText}
          </HiraginoKakuText>
        </View>
      </Pressable>
    </View>
  );
};
