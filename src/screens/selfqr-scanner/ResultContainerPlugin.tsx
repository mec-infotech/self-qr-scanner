import React from 'react';

interface Result {
    decodedText: string;
    result: {
        format: {
            formatName: string;
        };
    };
}

function filterResults(results: Result[]): Result[] {
    let filteredResults: Result[] = [];
    for (let i = 0; i < results.length; ++i) {
        if (i === 0) {
            filteredResults.push(results[i]);
            continue;
        }

        if (results[i].decodedText !== results[i - 1].decodedText) {
            filteredResults.push(results[i]);
        }
    }
    return filteredResults;
}

const ResultContainerTable: React.FC<{ data: Result[] }> = ({ data }) => {
    return (
        <table className={'Qrcode-result-table'}>
            <thead>
                <tr>
                    <td>#</td>
                    <td>Decoded Text</td>
                    <td>Format</td>
                </tr>
            </thead>
            <tbody>
                {
                    data.map((result, i) => {
                        console.log(result);
                        return (
                            <tr key={i}>
                                <td>{i}</td>
                                <td>{result.decodedText}</td>
                                <td>{result.result && result.result.format && result.result.format.formatName}</td>
                            </tr>
                        );
                    })
                }
            </tbody>
        </table>
    );
};

const ResultContainerPlugin: React.FC<{ results: Result[] }> = ({ results }) => {
    const filteredResults = filterResults(results);
    return (
        <div className='Result-container'>
            <div className='Result-header'>Scanned results ({filteredResults.length})</div>
            <div className='Result-section'>
                <ResultContainerTable data={filteredResults} />
            </div>
        </div>
    );
};

export default ResultContainerPlugin;
