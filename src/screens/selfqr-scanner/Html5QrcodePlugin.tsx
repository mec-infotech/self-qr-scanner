import React, { useState, useEffect } from "react";
import {
  View,
  Image,
  SafeAreaView,
  StatusBar,
  Text,
  Alert,
  Pressable,
  Platform,
  Modal,
  Button,
} from "react-native";
import { Camera } from "expo-camera";
import { Header } from "../../components/basics/header";
import { Footer } from "../../components/basics/footer";
import { styles } from "./SelfqrScannerStyles";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { HiraginoKakuText } from "../../components/StyledText";
import { Html5QrcodeScanType, Html5QrcodeScanner } from 'html5-qrcode';

type Props = {
  navigation: NavigationProp<any, any>;
};

type Params = {
  eventId: number,
  venueId: number,
  receptMethodCode: number,
}

export const SelfqrScanner = ({ navigation }: Props) => {
  const route = useRoute();
  const { eventId, venueId, receptMethodCode } = route.params as Params;

  const [hasPermission, setHasPermission] = useState<boolean | null>(null);
  const [scanned, setScanned] = useState(false);
  const [decodedResults, setDecodedResults] = useState<string[]>([]);
  const [isScannerReady, setIsScannerReady] = useState<boolean>(false);
  let qrCodeScanner: Html5QrcodeScanner | undefined;

  useEffect(() => {
    if (hasPermission && Platform.OS !== 'ios') {
      setIsScannerReady(true);
    }
  }, [hasPermission]);

  useEffect(() => {
    if (isScannerReady) {
      const config = {
        fps: 10,
        rememberLastUsedCamera: true,
        supportedScanTypes: [Html5QrcodeScanType.SCAN_TYPE_CAMERA],
      };

      qrCodeScanner = new Html5QrcodeScanner(
        "reader",
        config,
        false
      );
      qrCodeScanner.render(qrCodeSuccessCallback, qrCodeErrorCallback);
    }

    (async () => {
      const { status } = await Camera.requestCameraPermissionsAsync();
      setHasPermission(status === "granted");
    })();
  }, [isScannerReady]);

  const qrCodeSuccessCallback = (decodedText: string) => {
    console.log("Decoded text:", decodedText);
    if (!scanned) {
      setScanned(true);
    }
    setDecodedResults((prev) => [...prev, decodedText]);
  };

  const qrCodeErrorCallback = (errorMessage: string) => {
    // console.error("QR code scan error:", errorMessage);
    // Handle QR code scan error
  };

  const handleBarcodeScanned = ({ data }: { data: string }) => {
    if (!scanned) {
      setScanned(true);
      Alert.alert(
        "QR Code Scanned",
        `Bar code with data ${data} has been scanned!`,
        [
          {
            text: "OK",
            onPress: () => {
              setScanned(false);
            },
          },
        ],
        { cancelable: false }
      );
    }
  };

  if (hasPermission === null) {
    return <Text>Requesting camera permission...</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  const handleReturnButton = () => {
    navigation.navigate("SelfqrDescription", {
      userId: "mec",
    });
  };

  const handleCheckInConfirmation = () => {
    navigation.navigate("CheckInConfirmation", {
      // userId: "mec",
      eventId: eventId,
      venueId: venueId,
      receptMethodCode: receptMethodCode,
      surname: "太郎",
      givenName: "出茂",
      kanaSurname: "タロウ",
      kanaGivenName: "イズモ",
      birthDate: "2000年01月02日",
      genderCode: 1,
      postCode: "515-0004",
      address: "三重県松阪市なんとか町11-2　マンション名102あああああああああああああああああ",
    });
  };

  const handleGroupCheckInConfirmation = () => {
    navigation.navigate("GroupCheckInConfirmation", {
      userId: "mec",
    });
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  const closeModal = () => {
    setScanned(false);
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="自己QRをかざしてください"
        buttonName="受付をやめる"
        onPress={handleSelectReceptionMethod}
      />
      <View style={styles.container}>
        <View style={styles.leftSide}>
          <Image
            source={require("../../assets/images/qrScanner.png")}
            style={styles.image}
          />
        </View>
        {hasPermission && (
          Platform.OS === 'ios' ? (

            <View style={styles.rightSide}>
              <Camera
                style={styles.camera}
                type={"back" as any}
                onBarCodeScanned={handleBarcodeScanned}
              />

              <View style={[styles.corner, styles.topLeftCorner]} />
              <View style={[styles.corner, styles.topRightCorner]} />
              <View style={[styles.corner, styles.bottomLeftCorner]} />
              <View style={[styles.corner, styles.bottomRightCorner]} />
              <View style={styles.overlay}>
                <View style={styles.buttonContainer}>
                  <Pressable
                    style={styles.receptionByOne}
                    onPress={handleCheckInConfirmation}
                  >
                    <HiraginoKakuText normal>1人で受付</HiraginoKakuText>
                  </Pressable>
                  <Pressable
                    style={styles.receptionByFamily}
                    onPress={handleGroupCheckInConfirmation}
                  >
                    <HiraginoKakuText normal>家族で受付</HiraginoKakuText>
                  </Pressable>
                </View>
              </View>
            </View>
          ) : (
            <View style={styles.rightSide}>
              <View style={styles.webCamContainer}>
                <View id="reader" style={styles.webCamera} />
              </View>

              <View style={[styles.corner, styles.topLeftCornerWeb]} />
              <View style={[styles.corner, styles.topRightCornerWeb]} />
              <View style={[styles.corner, styles.bottomLeftCornerWeb]} />
              <View style={[styles.corner, styles.bottomRightCornerWeb]} />
              <View style={styles.overlay}>
                <View style={styles.buttonContainer}>
                  <Pressable
                    style={styles.receptionByOne}
                    onPress={handleCheckInConfirmation}
                  >
                    <HiraginoKakuText normal>1人で受付</HiraginoKakuText>
                  </Pressable>
                  <Pressable
                    style={styles.receptionByFamily}
                    onPress={handleGroupCheckInConfirmation}
                  >
                    <HiraginoKakuText normal>家族で受付</HiraginoKakuText>
                  </Pressable>
                </View>
              </View>
            </View>
          )
        )}
        <Modal
          visible={scanned}
          onRequestClose={closeModal}
          animationType="slide"
        >
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>QR Code Scanned</Text>
            <Text>Bar code with data {decodedResults[decodedResults.length - 1]} has been scanned!</Text>
            <Button title="OK" onPress={closeModal} />
          </View>
        </Modal>


      </View>
      <Footer
        hasNextButton={false}
        onPressPrevious={handleReturnButton}
      ></Footer>
    </SafeAreaView>
  );
};
