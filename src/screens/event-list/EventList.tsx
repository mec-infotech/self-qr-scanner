import React, { useState, useRef } from "react";
import {
  StatusBar,
  SafeAreaView,
  TextInput,
  Pressable,
  View,
  TouchableWithoutFeedback,
  Keyboard,
  Dimensions,
  ScrollView,
} from "react-native";
import styles from "./EventListStyle";
import { Header } from "../../components/basics/header";
import { Button } from "../../components/basics/Button";
import { HiraginoKakuText } from "../../components/StyledText";
import { colors } from "../../styles/color";
import {
  Entypo,
  MaterialIcons,
  MaterialCommunityIcons,
  Feather,
} from "@expo/vector-icons";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { Logout } from "../logout/Logout";
import { format } from "date-fns";
import { CustomCalendar } from "../../components/basics/Calendar";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

type Props = {
  navigation: NavigationProp<any, any>;
};
export const EventList = ({ navigation }: Props) => {
  const [showInputs, setShowInputs] = useState(false);
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);
  const [isLogoutModalVisible, setLogoutModalVisible] = useState(false);
  const [selectedOption, setSelectedOption] = useState("最終更新日が新しい");
  const [isFromStartDateCalendarVisible, setFromStartDateCalendarVisible] =
    useState(false);
  const [isToStartDateCalendarVisible, setToStartDateCalendarVisible] =
    useState(false);
  const [isFromEndDateCalendarVisible, setFromEndDateCalendarVisible] =
    useState(false);
  const [isToEndDateCalendarVisible, setToEndDateCalendarVisible] =
    useState(false);
  const [fromStartDate, setFromStartDate] = useState("");
  const [toStartDate, setToStartDate] = useState("");
  const [fromEndDate, setFromEndDate] = useState("");
  const [toEndDate, setToEndDate] = useState("");
  const fromStartDateInputRef = useRef(null);
  const toStartDateInputRef = useRef(null);
  const fromEndDateInputRef = useRef(null);
  const toEndDateInputRef = useRef(null);
  const fromStartDateRef = useRef(null);
  const toStartDateRef = useRef(null);
  const fromEndDateRef = useRef(null);
  const toEndDateRef = useRef(null);
  const { width } = Dimensions.get("window");
  const [x, setX] = useState(0);
  const [focusedInput, setFocusedInput] = useState("");
  const [eventTitle, setEventTitle] = useState("");
  const [venueText, setVenueText] = useState("");

  const defaultOnPress = (event: any) => {
    handleClosePopup(event);
  };

  const toggleInputs = (event: any) => {
    setShowInputs(!showInputs);
    handleClosePopup(event);
  };

  const handleEventTitleChange = (text: string) => {
    setEventTitle(text);
  };

  const handleVenueTextChange = (text: string) => {
    setVenueText(text);
  };

  // Clear Button Action
  const handleClearPress = (event: any) => {
    handleClosePopup(event);
    setEventTitle("");
    setVenueText("");
    setFromStartDate("");
    setToStartDate("");
    setFromEndDate("");
    setToEndDate("");
  };

  // Sorting Dropdown
  const handleDropdownPress = () => {
    setIsDropdownVisible(!isDropdownVisible);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
  };

  const dropdownData = [
    { label: "最終更新日が新しい", value: "newest" },
    { label: "最終更新日が古い", value: "oldest" },
  ];

  const handleDropdownSelect = (value: any) => {
    setSelectedOption(value);
    setIsDropdownVisible(false);
  };

  const handleEventDetail = (eventItems: any) => {
    if (isDropdownVisible) {
      setIsDropdownVisible(false);
    } else {
      setIsDropdownVisible(false);
      navigation.navigate("EventDetail", {
        userId: "mec",
        eventName: eventItems.eventName,
        eventPeriod: eventItems.eventPeriod,
        eventId: eventItems.id,
      });
    }
  };

  // Close dropdown when click outside
  const handleClosePopup = (event: any) => {
    if (isDropdownVisible) {
      setIsDropdownVisible(false);
    } else {
      closeCalendar(event);
    }
  };

  const handleFromStartDateCalendarPress = (event: any) => {
    (fromStartDateInputRef.current as any).focus();
    setFromStartDateCalendarVisible(!isFromStartDateCalendarVisible);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    setIsDropdownVisible(false);
  };

  const handleToStartDateCalendarPress = (event: any) => {
    (toStartDateInputRef.current as any).focus();
    setToStartDateCalendarVisible(!isToStartDateCalendarVisible);
    setFromStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    setIsDropdownVisible(false);
  };

  const handleFromEndDateCalendarPress = (event: any) => {
    (fromEndDateInputRef.current as any).focus();
    setFromEndDateCalendarVisible(!isFromEndDateCalendarVisible);
    setToEndDateCalendarVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setIsDropdownVisible(false);
    setFocusedInput("fromEndDateInput");
  };

  const handleToEndDateCalendarPress = (event: any) => {
    (toEndDateInputRef.current as any).focus();
    setFromEndDateCalendarVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    changeXPosition();
    setToEndDateCalendarVisible(!isToEndDateCalendarVisible);
    setIsDropdownVisible(false);
    setFocusedInput("toEndDateInput");
  };

  const changeXPosition = () => {
    (toEndDateInputRef.current as any).measure(
      (
        x: number,
        y: number,
        width: number,
        height: number,
        pageX: number,
        pageY: number
      ) => {
        setX(pageX);
      }
    );
  };

  const handleFromStartDateSelect = (date: any) => {
    setFromStartDate(date);
    setFromStartDateCalendarVisible(false);
  };

  const handleToStartDateSelect = (date: any) => {
    setToStartDate(date);
    setToStartDateCalendarVisible(false);
  };

  const handleFromEndDateSelect = (date: any) => {
    setFromEndDate(date);
    setFromEndDateCalendarVisible(false);
  };

  const handleToEndDateSelect = (date: any) => {
    setToEndDate(date);
    setToEndDateCalendarVisible(false);
  };

  const closeCalendar = (event: any) => {
    if (
      event.nativeEvent.target != fromStartDateInputRef.current &&
      event.nativeEvent.target != fromStartDateRef.current
    ) {
      if (isFromStartDateCalendarVisible) {
        setFromStartDateCalendarVisible(false);
      }
    }
    if (
      event.nativeEvent.target != toStartDateInputRef.current &&
      event.nativeEvent.target != toStartDateRef.current
    ) {
      if (isToStartDateCalendarVisible) {
        setToStartDateCalendarVisible(false);
      }
    }
    if (
      event.nativeEvent.target != fromEndDateInputRef.current &&
      event.nativeEvent.target != fromEndDateRef.current
    ) {
      if (isFromEndDateCalendarVisible) {
        setFromEndDateCalendarVisible(false);
      }
    }
    if (
      event.nativeEvent.target != toEndDateInputRef.current &&
      event.nativeEvent.target != toEndDateRef.current
    ) {
      if (isToEndDateCalendarVisible) {
        setToEndDateCalendarVisible(false);
      }
    }
  };

  // Pagination
  const DATA = [
    {
      id: 1,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場",
      eventPeriod: "2023/05/12 〜 2023/05/12",
    },
    {
      id: 2,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場、C会場、D会場",
      eventPeriod: "2023/05/11 〜 2023/05/11",
    },
    {
      id: 3,
      eventName: "出茂マラソン大会2024",
      venue: "会場1",
      eventPeriod: "",
    },
    {
      id: 4,
      eventName:
        "出茂マラソン大会出茂マラソン大会出茂マラソン大会出茂マラソン大会出茂マラソン大会出茂マラソン大会",
      venue: "会場1",
      eventPeriod: "2023/04/7 〜 2023/04/7",
    },
    {
      id: 5,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場",
      eventPeriod: "2023/03/12 〜 2023/03/12",
    },
    {
      id: 6,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場",
      eventPeriod: "2023/02/7 〜 2023/02/7",
    },
    {
      id: 7,
      eventName: "出茂マラソン大会2023",
      venue: "会場2",
      eventPeriod: "2023/01/15 〜 2023/01/15",
    },
    {
      id: 8,
      eventName: "出茂マラソン大会",
      venue: "C会場",
      eventPeriod: "2023/01/5 〜 2023/01/5",
    },
  ];

  const ITEMS_PER_PAGE = 6;
  const [page, setPage] = useState(1);
  const totalCount = DATA.length.toLocaleString();

  // Search Button Action
  const [filteredData, setFilteredData] = useState([...DATA]);
  const handleSearchPress = (event: any) => {
    handleClosePopup(event);
    // Filter data based on the inputted text in eventTitle
    // const filtered = DATA.filter(
    //   (item) =>
    //     item.eventName.includes(eventTitle) || item.venue.includes(venueText)
    //   // ||
    //   // item.eventPeriod.includes(eventTitle)
    // );
    // setFilteredData(filtered);

    // const filtered = DATA.filter(
    //   (item) =>
    //     item.eventName.includes(eventTitle) && item.venue.includes(venueText)
    // );
    // Apply filtering based on event title and venue text
    let filteredData = DATA.filter(
      (item) =>
        item.eventName.toLowerCase().includes(eventTitle.toLowerCase()) &&
        item.venue.toLowerCase().includes(venueText.toLowerCase())
    );
    setFilteredData(filteredData);
  };

  const renderHeader = () => (
    <View style={styles.header}>
      <HiraginoKakuText style={[styles.headerText, styles.eventHeaderText]}>
        イベント名
      </HiraginoKakuText>
      <HiraginoKakuText style={[styles.headerText, styles.venueHeaderText]}>
        会場
      </HiraginoKakuText>
      <HiraginoKakuText style={[styles.headerText, styles.eventDateText]}>
        イベント期間
      </HiraginoKakuText>
    </View>
  );

  const renderTableItem = ({ item }: { item: any }) => (
    <Pressable style={styles.row} onPress={() => handleEventDetail(item)}>
      <HiraginoKakuText
        style={[
          styles.cell,
          styles.eventBodyText,
          styles.eventBodyPressableText,
        ]}
        numberOfLines={1}
        normal
      >
        {item.eventName}
      </HiraginoKakuText>
      <HiraginoKakuText
        style={[styles.cell, styles.venueBodyText]}
        numberOfLines={1}
        normal
      >
        {item.venue}
      </HiraginoKakuText>
      <HiraginoKakuText style={[styles.cell, styles.eventDateBodyText]} normal>
        {item.eventPeriod}
      </HiraginoKakuText>
    </Pressable>
  );

  // const getPageData = () => {
  //   let sortedData = [...filteredData];
  //   if (selectedOption === "最終更新日が古い") {
  //     sortedData.sort((a, b) => {
  //       const dateA: Date = new Date(a.eventPeriod.split(" ")[0]);
  //       const dateB: Date = new Date(b.eventPeriod.split(" ")[0]);
  //       return dateA.getTime() - dateB.getTime();
  //     });
  //   } else if (selectedOption === "最終更新日が新しい") {
  //     sortedData.sort((a, b) => {
  //       const dateA: Date = new Date(a.eventPeriod.split(" ")[0]);
  //       const dateB: Date = new Date(b.eventPeriod.split(" ")[0]);
  //       return dateB.getTime() - dateA.getTime();
  //     });
  //   }
  //   const startIndex = (page - 1) * ITEMS_PER_PAGE;
  //   const endIndex = startIndex + ITEMS_PER_PAGE;
  //   return sortedData.slice(startIndex, endIndex);
  // };
  const getPageData = () => {
    let sortedData = [...filteredData];
    if (selectedOption === "最終更新日が古い") {
      sortedData.sort((a, b) => {
        const dateA: Date = new Date(a.eventPeriod.split(" ")[0]);
        const dateB: Date = new Date(b.eventPeriod.split(" ")[0]);
        return dateA.getTime() - dateB.getTime();
      });
    } else if (selectedOption === "最終更新日が新しい") {
      sortedData.sort((a, b) => {
        const dateA: Date = new Date(a.eventPeriod.split(" ")[0]);
        const dateB: Date = new Date(b.eventPeriod.split(" ")[0]);
        return dateB.getTime() - dateA.getTime();
      });
    }

    const startIndex = (page - 1) * ITEMS_PER_PAGE;
    const endIndex = startIndex + ITEMS_PER_PAGE;
    return sortedData.slice(startIndex, endIndex);
  };

  // Logout Button action
  const route = useRoute();
  const { userId } = route.params as { userId: string };

  const handleLogout = () => {
    openLogoutModal();
  };

  const openLogoutModal = () => {
    setIsDropdownVisible(false);
    setLogoutModalVisible(true);
  };

  const handleLogOutButton = () => {
    navigation.navigate("Login", {
      userId: userId,
    });
  };

  const handleLogOutCancelButton = () => {
    setLogoutModalVisible(false);
  };

  const handleEventTitleFocus = (event: any) => {
    handleClosePopup(event);
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="受付するイベントを選んでください"
        buttonName="ログアウト"
        onPress={handleLogout}
      />
      <ScrollView>
        <TouchableWithoutFeedback onPress={handleClosePopup}>
          <View style={styles.bodyContainer}>
            <View
              style={[styles.firstChildContainer, { gap: showInputs ? 16 : 8 }]}
            >
              <View style={styles.infoContainer}>
                <View style={styles.parentInputContainer}>
                  <View style={styles.childInputContainer}>
                    <View style={styles.labelContainer}>
                      <HiraginoKakuText style={styles.labelText}>
                        イベント名
                      </HiraginoKakuText>
                    </View>
                    <View style={styles.inputContainer}>
                      <TextInput
                        style={styles.input}
                        placeholder="イベントタイトル"
                        placeholderTextColor={colors.placeholderTextColor}
                        onFocus={handleEventTitleFocus}
                        onChangeText={handleEventTitleChange}
                        value={eventTitle}
                      />
                    </View>
                  </View>
                </View>
                <>
                  <Pressable
                    style={styles.dropdownContainer}
                    onPress={toggleInputs}
                  >
                    <HiraginoKakuText style={styles.dropdownText}>
                      詳細検索
                    </HiraginoKakuText>
                    <Entypo
                      name={showInputs ? "chevron-up" : "chevron-down"}
                      size={24}
                      color={colors.primary}
                      style={styles.iconStyle}
                    />
                  </Pressable>
                  {showInputs && (
                    <View style={styles.hiddenContainer}>
                      <View style={styles.parentInputContainer}>
                        <View style={styles.childInputContainer}>
                          <View style={styles.labelContainer}>
                            <HiraginoKakuText style={styles.labelText}>
                              会場
                            </HiraginoKakuText>
                          </View>
                          <View style={styles.inputContainer}>
                            <TextInput
                              style={styles.input}
                              placeholder="会場名"
                              placeholderTextColor={colors.placeholderTextColor}
                              onFocus={handleClosePopup}
                              onChangeText={handleVenueTextChange}
                              value={venueText}
                            />
                          </View>
                        </View>
                      </View>
                      <View style={[styles.mainDateContainer]}>
                        <View style={styles.parentDateContainer}>
                          <View style={styles.labelContainer}>
                            <HiraginoKakuText style={styles.labelText}>
                              開始日
                            </HiraginoKakuText>
                          </View>
                          <View style={styles.childDateContainer}>
                            <View style={styles.dateInputContainer}>
                              <TextInput
                                ref={fromStartDateInputRef}
                                style={styles.dateInput}
                                placeholder="日付を選択"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                                value={
                                  fromStartDate != ""
                                    ? format(
                                        new Date(fromStartDate),
                                        "yyyy/MM/dd"
                                      )
                                    : fromStartDate
                                }
                                onPressIn={handleFromStartDateCalendarPress}
                                onPointerDown={handleFromStartDateCalendarPress}
                                showSoftInputOnFocus={false}
                                onTouchStart={() => Keyboard.dismiss()}
                                editable={false}
                              />
                              <Pressable
                                ref={fromStartDateRef}
                                style={styles.calendarIconContainer}
                                onPress={handleFromStartDateCalendarPress}
                              >
                                <MaterialIcons
                                  name="calendar-today"
                                  size={24}
                                  color={colors.activeCarouselColor}
                                />
                              </Pressable>
                              {isFromStartDateCalendarVisible && (
                                <CustomCalendar
                                  selectedDate={fromStartDate}
                                  onDateSelect={handleFromStartDateSelect}
                                />
                              )}
                            </View>
                            <HiraginoKakuText style={styles.tildeText}>
                              〜
                            </HiraginoKakuText>
                            <View style={styles.secondDateInputContainer}>
                              <TextInput
                                ref={toStartDateInputRef}
                                style={styles.dateInput}
                                placeholder="日付を選択"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                                value={
                                  toStartDate != ""
                                    ? format(
                                        new Date(toStartDate),
                                        "yyyy/MM/dd"
                                      )
                                    : toStartDate
                                }
                                onPressIn={handleToStartDateCalendarPress}
                                onPointerDown={handleToStartDateCalendarPress}
                                showSoftInputOnFocus={false}
                                onTouchStart={() => Keyboard.dismiss()}
                                editable={false}
                              />
                              <Pressable
                                ref={toStartDateRef}
                                onPress={handleToStartDateCalendarPress}
                                style={styles.calendarIconContainer}
                              >
                                <MaterialIcons
                                  name="calendar-today"
                                  size={24}
                                  color={colors.activeCarouselColor}
                                />
                              </Pressable>
                              {isToStartDateCalendarVisible && (
                                <CustomCalendar
                                  selectedDate={toStartDate}
                                  onDateSelect={handleToStartDateSelect}
                                />
                              )}
                            </View>
                          </View>
                        </View>
                        <View style={styles.secondParentDateContainer}>
                          <View style={styles.labelContainer}>
                            <HiraginoKakuText style={styles.labelText}>
                              終了日
                            </HiraginoKakuText>
                          </View>
                          <View style={styles.childDateContainer}>
                            <View
                              style={[
                                styles.dateInputContainer,
                                x + wp("30.25%") > width &&
                                  focusedInput === "toEndDateInput" && {
                                    zIndex: -1,
                                  },
                              ]}
                            >
                              <TextInput
                                ref={fromEndDateInputRef}
                                style={styles.dateInput}
                                placeholder="日付を選択"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                                value={
                                  fromEndDate != ""
                                    ? format(
                                        new Date(fromEndDate),
                                        "yyyy/MM/dd"
                                      )
                                    : fromEndDate
                                }
                                onPressIn={handleFromEndDateCalendarPress}
                                onPointerDown={handleFromEndDateCalendarPress}
                                showSoftInputOnFocus={false}
                                onTouchStart={() => Keyboard.dismiss()}
                                editable={false}
                              />
                              <Pressable
                                ref={fromEndDateRef}
                                style={styles.calendarIconContainer}
                                onPress={handleFromEndDateCalendarPress}
                              >
                                <MaterialIcons
                                  name="calendar-today"
                                  size={24}
                                  color={colors.activeCarouselColor}
                                />
                              </Pressable>
                              {isFromEndDateCalendarVisible && (
                                <CustomCalendar
                                  selectedDate={fromEndDate}
                                  onDateSelect={handleFromEndDateSelect}
                                />
                              )}
                            </View>
                            <HiraginoKakuText style={styles.tildeText}>
                              〜
                            </HiraginoKakuText>
                            <View
                              style={[
                                styles.secondDateInputContainer,
                                x + wp("30.25%") > width && {
                                  justifyContent: "flex-end",
                                },
                                x + wp("30.25%") > width &&
                                  focusedInput === "fromStartDateInput" && {
                                    zIndex: -1,
                                  },
                                { transform: "10" },
                              ]}
                            >
                              <TextInput
                                ref={toEndDateInputRef}
                                style={styles.dateInput}
                                placeholder="日付を選択"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                                value={
                                  toEndDate != ""
                                    ? format(new Date(toEndDate), "yyyy/MM/dd")
                                    : toEndDate
                                }
                                onPressIn={handleToEndDateCalendarPress}
                                onPointerDown={handleToEndDateCalendarPress}
                                showSoftInputOnFocus={false}
                                onTouchStart={() => Keyboard.dismiss()}
                                editable={false}
                              />
                              <Pressable
                                ref={toEndDateRef}
                                style={styles.calendarIconContainer}
                                onPress={handleToEndDateCalendarPress}
                              >
                                <MaterialIcons
                                  name="calendar-today"
                                  size={24}
                                  color={colors.activeCarouselColor}
                                />
                              </Pressable>
                              {isToEndDateCalendarVisible && (
                                <CustomCalendar
                                  selectedDate={toEndDate}
                                  onDateSelect={handleToEndDateSelect}
                                />
                              )}
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  )}
                </>
              </View>
              <View style={styles.buttonContainer}>
                <Button
                  text="クリア"
                  onPress={handleClearPress}
                  style={styles.grayMButton}
                  type="ButtonMediumGray"
                />
                <Button
                  text="検索"
                  onPress={handleSearchPress}
                  style={styles.PrimaryMButton}
                  type="ButtonMPrimary"
                />
              </View>
            </View>
            <View style={styles.mainPaginationContainer}>
              <View style={styles.parentPaginationContainer}>
                <View style={styles.topPaginationContainer}>
                  <View style={styles.countContainer}>
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      1-{ITEMS_PER_PAGE} / {totalCount} 件中
                    </HiraginoKakuText>
                  </View>
                  <Pressable
                    style={styles.sortingContainer}
                    onPress={handleDropdownPress}
                  >
                    <MaterialCommunityIcons
                      name="swap-vertical"
                      size={20}
                      color={colors.activeCarouselColor}
                    />
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      {selectedOption}
                    </HiraginoKakuText>
                    <Entypo
                      name="chevron-down"
                      size={20}
                      color={colors.greyTextColor}
                      style={styles.dropdownIconStyle}
                    />
                  </Pressable>

                  {/* Sorting Dropdown */}
                  {isDropdownVisible && (
                    <View style={styles.dropdown}>
                      {dropdownData.map((item) => (
                        <Pressable
                          key={item.value}
                          style={styles.dropdownItem}
                          onPress={() => handleDropdownSelect(item.label)}
                        >
                          <HiraginoKakuText
                            style={styles.paginationCount}
                            normal
                          >
                            {item.label}
                          </HiraginoKakuText>
                        </Pressable>
                      ))}
                    </View>
                  )}
                </View>
                <View style={styles.tableContainer}>
                  {/* Render the header */}
                  {renderHeader()}

                  {/* Render the table items */}
                  {getPageData().map((item, index) => (
                    <View key={index}>{renderTableItem({ item })}</View>
                  ))}
                </View>
              </View>
              <View style={styles.onChangePageContainer}>
                <View style={styles.previousButtonsContainer}>
                  <Button
                    text=""
                    onPress={defaultOnPress}
                    style={styles.skipBackButton}
                    type="ButtonSDisable"
                    icon={
                      <Feather
                        name="skip-back"
                        size={20}
                        color={colors.greyTextColor}
                      />
                    }
                    iconPosition="center"
                  />
                  <Button
                    text=""
                    onPress={defaultOnPress}
                    style={styles.chevronLeftButton}
                    type="ButtonSDisable"
                    icon={
                      <Feather
                        name="chevron-left"
                        size={20}
                        color={colors.greyTextColor}
                      />
                    }
                    iconPosition="center"
                  />
                </View>
                <View style={styles.pageNumberContainer}>
                  <Button
                    text="1"
                    onPress={defaultOnPress}
                    style={styles.numOneButton}
                    type="ButtonSPrimary"
                  />
                  <Button
                    text="2"
                    onPress={defaultOnPress}
                    style={styles.numSGrayButton}
                    type="ButtonSGray"
                  />
                  <Button
                    text="3"
                    onPress={defaultOnPress}
                    style={styles.numSGrayButton}
                    type="ButtonSGray"
                  />
                  <HiraginoKakuText style={styles.threeDots} normal>
                    …
                  </HiraginoKakuText>
                  <Button
                    text="50"
                    onPress={defaultOnPress}
                    style={styles.numSGrayButton}
                    type="ButtonSGray"
                  />
                </View>
                <View style={styles.nextButtonsContainer}>
                  <Button
                    text=""
                    onPress={defaultOnPress}
                    style={styles.rightButtons}
                    type="ButtonSGray"
                    icon={
                      <Feather
                        name="chevron-right"
                        size={20}
                        color={colors.textColor}
                      />
                    }
                    iconPosition="center"
                  />
                  <Button
                    text=""
                    onPress={defaultOnPress}
                    style={styles.rightButtons}
                    type="ButtonSGray"
                    icon={
                      <Feather
                        name="skip-forward"
                        size={20}
                        color={colors.textColor}
                      />
                    }
                    iconPosition="center"
                  />
                </View>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>

      {isLogoutModalVisible && (
        <Logout
          onLogOutButtonPress={handleLogOutButton}
          onCancelButtonPress={handleLogOutCancelButton}
        />
      )}
    </SafeAreaView>
  );
};
