import React, { useState, useRef } from "react";
import { StatusBar, SafeAreaView, Pressable, TextInput, Keyboard, TouchableWithoutFeedback } from "react-native";
import styles from "./CheckInStyle";
import { Header } from "../../components/basics/header";
import { View } from "../../components/Themed";
import { Footer } from "../../components/basics/footer";
import { HiraginoKakuText } from "../../components/StyledText";
import { MaterialIcons } from "@expo/vector-icons";
import { colors } from "../../styles/color";
import { Button } from "../../components/basics/Button";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { CustomCalendar } from "../../components/basics/Calendar";
import { format } from "date-fns";

type Props = {
  navigation: NavigationProp<any, any>;
};

type Params = {
  eventId: number,
  venueId: number,
  receptMethodCode: number,
}

export const CheckIn = ({ navigation }: Props) => {
  const route = useRoute();
  let { eventId, venueId, receptMethodCode } = route.params as Params

  console.log("EventId: ", eventId, "VenueId: ", venueId, "受付方法コード： ", receptMethodCode);

  const [selectedOption, setSelectedOption] = useState("");
  const [isBirthDateCalendarVisible, setBirthDateCalendarVisible] = useState(false);
  const [birthDate, setBirthDate] = useState("");
  const birthDateInputRef = useRef(null);
  const birthDateRef = useRef(null);

  const handleSelectOption = (option: string) => {
    setSelectedOption((prevOption) => (prevOption === option ? prevOption : option));
  };

  const defaultOnPress = () => { };

  const handleReturn = () => {
    navigation.navigate("SelectReceptionMethod", {
      eventId: eventId,
      venueId: venueId,
    });
  };

  const handleCheckInConfirmation = () => {
    navigation.navigate("CheckInConfirmation", {
      userId: "mec",
    });
  };

  const handleSelectReceptionMethod = () => {
    navigation.navigate("SelectReceptionMethod", {
      userId: "mec",
    });
  };

  const handleBirthDateCalendarPress = (event: any) => {
    (birthDateInputRef.current as any).focus();
    setBirthDateCalendarVisible(!isBirthDateCalendarVisible);
  };

  const handleBirthDateSelect = (date: any) => {
    setBirthDate(date);
    setBirthDateCalendarVisible(false);
  };

  const closeCalendar = (event: any) => {
    if (event.nativeEvent.target != birthDateInputRef.current && event.nativeEvent.target != birthDateRef) {
      if (isBirthDateCalendarVisible) {
        setBirthDateCalendarVisible(false);
      }
    }
  };

  return (
    <KeyboardAwareScrollView
      style={{ flex: 1, width: "100%" }}
      resetScrollToCoords={{ x: 0, y: 0 }}
      contentContainerStyle={styles.mainContainer}
      scrollEnabled={false}
    >
      <TouchableWithoutFeedback onPress={closeCalendar}>
        <SafeAreaView style={styles.mainContainer}>
          <StatusBar barStyle="dark-content" />
          <Header
            titleName="この場で入力して受付"
            buttonName="受付をやめる"
            onPress={handleSelectReceptionMethod}
          />
          <View style={styles.bodyContainer}>
            <HiraginoKakuText style={styles.titleText}>
              受付情報を入力してください
            </HiraginoKakuText>
            <View style={styles.container}>
              <View style={styles.nameLabelContainer}>
                <HiraginoKakuText style={styles.labelNameText}>
                  お名前
                </HiraginoKakuText>
                <View style={styles.nameInputsContainer}>
                  <TextInput
                    style={[styles.FirstNameInput, styles.FirstNameInputText]}
                    onFocus={() => setBirthDateCalendarVisible(false)}
                  />
                  <TextInput
                    style={[styles.LastNameInput, styles.LastNameInputText]}
                    onFocus={() => setBirthDateCalendarVisible(false)}
                  />
                </View>
              </View>
              <View style={styles.nameLabelContainer}>
                <HiraginoKakuText style={styles.labelNameText}>
                  お名前（カナ）
                </HiraginoKakuText>
                <View style={styles.nameInputsContainer}>
                  <TextInput
                    style={[styles.FirstNameInput, styles.FirstNameInputText]}
                    onFocus={() => setBirthDateCalendarVisible(false)}
                  />
                  <TextInput
                    style={[styles.LastNameInput, styles.LastNameInputText]}
                    onFocus={() => setBirthDateCalendarVisible(false)}
                  />
                </View>
              </View>
              <View style={styles.birthDateContainer}>
                <HiraginoKakuText style={styles.labelBirthDateText}>
                  生年月日
                </HiraginoKakuText>
                <View style={styles.birthDateInputsContainer}>
                  <View style={styles.birthDateInput}>
                    <TextInput
                      ref={birthDateInputRef}
                      style={styles.birthDateInputText}
                      value={birthDate != "" ? format(new Date(birthDate), "yyyy/MM/dd") : birthDate}
                      onPressIn={handleBirthDateCalendarPress}
                      onPointerDown={handleBirthDateCalendarPress}
                      showSoftInputOnFocus={false}
                      onTouchStart={() => Keyboard.dismiss()}
                      editable={false}
                    />
                    <Pressable style={styles.calendarIconContainer}
                      ref={birthDateRef}
                      onPress={handleBirthDateCalendarPress}>
                      <MaterialIcons
                        name="calendar-today"
                        size={22}
                        color={colors.activeCarouselColor}
                        style={styles.calendarIcon}
                      />
                    </Pressable>
                    {isBirthDateCalendarVisible && (
                      <CustomCalendar selectedDate={birthDate} onDateSelect={handleBirthDateSelect} />
                    )}
                  </View>
                </View>
              </View>
              <View style={styles.genderContainer}>
                <HiraginoKakuText style={styles.labelGenderText}>
                  性別
                </HiraginoKakuText>
                <View style={styles.genderRadioContainer}>
                  <View style={styles.radioContainer}>
                    <RadioPanel
                      selected={selectedOption === "M"}
                      onPress={() => handleSelectOption("M")}
                      radioBtnText="男性"
                    />
                  </View>
                  <View style={styles.radioContainer}>
                    <RadioPanel
                      selected={selectedOption === "F"}
                      onPress={() => handleSelectOption("F")}
                      radioBtnText="女性"
                    />
                  </View>
                  <View
                    style={[styles.radioContainer, styles.radioKaitouContainer]}
                  >
                    <RadioPanel
                      selected={selectedOption === "N"}
                      onPress={() => handleSelectOption("N")}
                      radioBtnText="回答しない"
                    />
                  </View>
                </View>
              </View>
              <View style={styles.postCodeContainer}>
                <HiraginoKakuText style={styles.labelPostCodeText}>
                  郵便番号
                </HiraginoKakuText>
                <View style={styles.postCodeInputsContainer}>
                  <TextInput
                    style={[styles.postCodeInput, styles.postCodeInputText]}
                    onFocus={() => setBirthDateCalendarVisible(false)}
                  />
                  <Button
                    text="住所検索"
                    onPress={defaultOnPress}
                    style={styles.searchButton}
                    type="ButtonMSecondary"
                  />
                </View>
              </View>
              <View style={styles.addressContainer}>
                <HiraginoKakuText style={styles.labelAddressText}>
                  住所
                </HiraginoKakuText>
                <TextInput
                  style={[styles.addressInput, styles.addressInputText]}
                  onFocus={() => setBirthDateCalendarVisible(false)}
                />
              </View>
            </View>
          </View>
          <Footer
            onPressPrevious={handleReturn}
            onPressNext={handleCheckInConfirmation}
            style={{ zIndex: -1 }}
          />
        </SafeAreaView>
      </TouchableWithoutFeedback>
    </KeyboardAwareScrollView>
  );
};

const RadioButton = (props: any) => {
  return (
    <View
      style={[
        {
          height: 24,
          width: 24,
          borderRadius: 12,
          borderWidth: 2,
          borderColor: colors.fullyTransparentBlack,
          alignItems: "center",
          justifyContent: "center",
        },
        props.style,
      ]}
    >
      {props.selected ? (
        <View
          style={{
            height: 10,
            width: 10,
            borderRadius: 6,
            backgroundColor: colors.primary,
          }}
        />
      ) : null}
    </View>
  );
};

interface RadioPanelProps {
  selected: boolean;
  onPress: () => void;
  radioBtnText: string;
}

const RadioPanel = ({ selected, onPress, radioBtnText }: RadioPanelProps) => {
  return (
    <Pressable onPress={onPress} style={styles.radioPressable}>
      <View style={styles.radioButtonIcon}>
        <RadioButton
          selected={selected}
          style={[styles.radioButton, selected && styles.selectedRadioButton]}
        />
      </View>
      <View style={styles.radioTextContainer}>
        <HiraginoKakuText style={styles.radioText} normal>
          {radioBtnText}
        </HiraginoKakuText>
      </View>
    </Pressable>
  );
};
