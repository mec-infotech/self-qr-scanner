interface LambdaResult {
    statusCode: number;
    body: string;
  }
  
  export async function invokeLambdaWithQuery(method: string, queryString: string): Promise<LambdaResult> {
    try {
      const baseURL = 'https://rvez22gifa.execute-api.eu-west-1.amazonaws.com/dev'; 
      const resource = "/rdsProxyPostgres";
      const url= baseURL + resource;
  
      const response = await fetch(url, {
        method: method,
        body: JSON.stringify({
          queryString
        })
      });
  
      const responseBody = await response.json();  
      // console.log('ResponseBody from API Gateway:', responseBody.body); 
  
      return {
        statusCode: responseBody.statusCode,
        body: responseBody.body
      };
    } catch (error:any) {
      console.error('Error invoking Lambda function through API Gateway:', error);
      return { statusCode: 500, body: JSON.stringify({ message: 'Internal server error', error: error.toString() }) };
    }
  }